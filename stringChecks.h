// stringChecks.h
// Jericho Keyne
//
// This provides prototypes for my stringCheck library
// These functions are useful for checking the contents of strings and characters

#include <string>
#include "stringChecks.cpp"

bool isUpperCase(char character);
bool isLowerCase(char character);
bool isLetter(char character);
bool isDigit(char character);
bool isValidSpecialCharacter(char character, string validCharacters);
bool containsUpperCase(string password);
bool containsLowerCase(string password);
bool containsDigit(string password);
bool containsValidSpecialCharacters(string password, string validCharacters);
bool containsInvalidSpecialCharacters(string password, string validCharacters);
bool containsRepeatingLetters(string password);
bool containsRepeatingDigits(string password);
bool containsSequentialDigits(string password);
