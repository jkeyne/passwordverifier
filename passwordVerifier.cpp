// Password Verifier
// Jericho Keyne
//
// This program will read in passwords one line at a time from passwords.txt
// It will then check the password against each of the requirements
// It will then output all the reasons why a password failed or if it is valid and move onto the next password

#include <iostream>
#include <string>
#include <fstream>
#include "stringChecks.h"

using namespace std;

string isValidPassword(string password, int minLength, int maxLength, string validCharacters);

int main() {
	// Declare variables
	string password;
	string returnStr;
	const int MAX_LENGTH = 15;
	const int MIN_LENGTH = 8;
	const string VALID_CHARACTERS = "!@#$%";
	ifstream passwordFile;
	passwordFile.open("passwords.txt");

	// This will fill the variable password with each line in passwordFile, one line at a time
	// This loop will end once it reaches the end of the file
	while (getline(passwordFile, password)) {
		cout << "The password is: " << password << endl;
		returnStr = isValidPassword(password, MIN_LENGTH, MAX_LENGTH, VALID_CHARACTERS);
		// If the password is valid we should continue onto the next iteration of the loop without
		// doing the other checks so that it will run a little more efficiently
		if (returnStr.length() == 0) {
			cout << "This password is valid\n\n";
			continue;
		}
		// string.find(char) will return string::npos when it can't find that character in the string
		if (returnStr.find('m') != string::npos)
			cout << "This password is too short\n";
		if (returnStr.find('M') != string::npos)
			cout << "This password is too long\n";
		if (returnStr.find('l') != string::npos)
			cout << "This password does not contain any lower case characters\n";
		if (returnStr.find('u') != string::npos)
			cout << "This password does not contain any upper case characters\n";
		if (returnStr.find('d') != string::npos)
			cout << "This password does not contain any digits\n";
		if (returnStr.find('v') != string::npos)
			cout << "This password does not contain any valid special characters\n";
		if (returnStr.find('i') != string::npos)
			cout << "This password contains at least one invalid special character\n";
		if (returnStr.find('L') != string::npos)
			cout << "This password contains repeating letters\n";
		if (returnStr.find('D') != string::npos)
			cout << "This password contains repeating digits\n";
		if (returnStr.find('s') != string::npos)
			cout << "This password contains sequential digits\n";
		cout << endl;
	}
	passwordFile.close();
	system("pause");
	return 0;
}

// This will run through all of the password requirements and return a string with each failed case
/*
* m = too short
* M = too long
* u = doesn't contain any upper case letters
* l = doesn't contain any lower case letters
* d = doesn't contain any digits
* v = doesn't contain any valid special characters
* i = contains invalid special characters
* L = contains repeating letters
* D = contains repeating digits
* s = contains sequential digits
*/
string isValidPassword(string password, int minLength, int maxLength, string validCharacters) {
	string returnStr="";
	if (password.length() < minLength)
		returnStr.append("m");
	if (password.length() > maxLength)
		returnStr.append("M");
	if (!containsUpperCase(password))
		returnStr.append("u");
	if (!containsLowerCase(password))
		returnStr.append("l");
	if (!containsDigit(password))
		returnStr.append("d");
	if (!containsValidSpecialCharacters(password, validCharacters))
		returnStr.append("v");
	if (containsInvalidSpecialCharacters(password, validCharacters))
		returnStr.append("i");
	if (containsRepeatingLetters(password))
		returnStr.append("L");
	if (containsRepeatingDigits(password))
		returnStr.append("D");
	if (containsSequentialDigits(password))
		returnStr.append("s");
	return returnStr;
}
