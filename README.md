# passwordVerifier

This program will read passwords (one per line) from a file called passwords.txt. It will then tell you if those passwords either meet the specifications or it will print all of the reasons that each password failed.

## isValidPassword
Here is what each character in the return string means:

Character | Meaning
--- | ---
m | Too short
M | Too long
u | There are no upper case letters
l | There are no lower case letters
d | There are no digits
v | There are no **valid** special characters
i | There is at least one **invalid** special character
L | There are repeating letters
D | There are repeating digits
s | There are sequential digits

# stringChecks
This is a library that has most of the functions for this program.

It contains a number of functions that do some basic checks on strings and characters

(eg. isUpperCase checks if a character is upper case, containsUpperCase checks if a string contains any upper case letters)
